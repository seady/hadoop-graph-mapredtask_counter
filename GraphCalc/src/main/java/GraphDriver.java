import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.Counters;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.RunningJob;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.*;


public class GraphDriver extends Configured implements Tool{

    @Override
    public int run(String[] args) throws Exception{
       

        String hadoopConfDir = System.getenv("HADOOP_HOME") +"/etc/hadoop/";
        
        Configuration conf = new Configuration();
        conf.addResource(new Path(hadoopConfDir+"mapred-site.xml"));
        conf.addResource(new Path(hadoopConfDir+"yarn-site.xml"));
        conf.addResource(new Path(hadoopConfDir+"hdfs-site.xml"));
        conf.addResource(new Path(hadoopConfDir+"core-site.xml"));
        setConf(conf);

        String uniquePath = getUniqueNameEx();
        Path tempDir = new Path(uniquePath);
        FileSystem fileSystem = FileSystem.get(getConf());

        if(fileSystem.exists(tempDir)){
            fileSystem.delete(tempDir,true);
        }

        Path outputDir = new Path(args[1]);
        if(fileSystem.exists(outputDir)){
            fileSystem.delete(outputDir,true);
        }
        fileSystem.mkdirs(outputDir);

        int k = Integer.parseInt(args[2]);


        Job graphJob = new Job(conf, "Graph Computation");
        graphJob.setJarByClass(GraphDriver.class);
        graphJob.setMapperClass(GraphMap.class);
        graphJob.setReducerClass(GraphReducer.class);
        graphJob.setCombinerClass(GraphCombiner.class);			

        graphJob.setOutputKeyClass(Text.class);
        graphJob.setOutputValueClass(IntWritable.class);

        FileInputFormat.addInputPath(graphJob, new Path(args[0]));	
        graphJob.setInputFormatClass(KeyValueTextInputFormat.class);	


        FileOutputFormat.setOutputPath(graphJob, new Path(args[1]));
        graphJob.setOutputFormatClass(TextOutputFormat.class);		

        // Execute graphJob and return status
        boolean success = graphJob.waitForCompletion(true);
	
	
        int edgeCount = 0;
        int vertices = 0;
        TreeMap<Integer,ArrayList<String>>  topKVertices = new TreeMap<Integer, ArrayList<String>>(Collections.reverseOrder());

        if(success){
            String job_id = graphJob.getJobID().toString();
            JobClient jobClient = new JobClient(new JobConf(getConf()));
            RunningJob runningJob = jobClient.getJob(job_id);
            Counters counters = runningJob.getCounters();
            edgeCount = (int) counters.findCounter("org.apache.hadoop.mapreduce.TaskCounter","MAP_INPUT_RECORDS").getValue();
            vertices = (int) counters.findCounter("org.apache.hadoop.mapreduce.TaskCounter","REDUCE_OUTPUT_RECORDS").getValue();

            FileStatus [] fileStatuses = fileSystem.listStatus(tempDir);

            for(FileStatus fileStatus:fileStatuses){
                if(fileStatus.isFile() && fileStatus.getPath().getName().startsWith("part")){
                    FSDataInputStream fileInputStream = fileSystem.open(fileStatus.getPath());
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream,"UTF-8"));
                    String line;
                    while ((line=bufferedReader.readLine())!=null){
                        Integer degree = Integer.parseInt(line.split("\t")[1]);
                        if(!topKVertices.containsKey(degree)){
                            ArrayList<String> verticeWithDegree = new ArrayList<String>();
                            verticeWithDegree.add(line);
                            topKVertices.put(degree,verticeWithDegree);
                        } else {
                            ArrayList<String> verticeWithDegree = topKVertices.get(degree);
                            verticeWithDegree.add(line);
                            topKVertices.put(degree,verticeWithDegree);
                        }
                    }
                    bufferedReader.close();
                    fileInputStream.close();
                }
            }

            FSDataOutputStream fout;
            fout = fileSystem.create(new Path(outputDir+"/output.txt"));
            BufferedWriter brout = new BufferedWriter(new OutputStreamWriter(fout,"UTF-8"));
            brout.write("Top K Vertices With Degree.....");
            brout.newLine();

            int count = 0;
            for(Map.Entry entry:topKVertices.entrySet()){
                ArrayList<String> verticeWithDegree = (ArrayList<String>) entry.getValue();
                for(String topVertice:verticeWithDegree){
                    if(count<k){
                        ++count;
                        brout.write(topVertice+System.lineSeparator());
                    }  else {
                        break;
                    }
                }
            }

            brout.write("All Vertices with Degree....");
            brout.newLine();

            for(FileStatus fileStatus:fileStatuses){
                if(fileStatus.isFile() && fileStatus.getPath().getName().startsWith("part")){
                    FSDataInputStream fileInputStream = fileSystem.open(fileStatus.getPath());
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream,"UTF-8"));
                    String line;
                    while ((line=bufferedReader.readLine())!=null){
                        brout.write(line+System.lineSeparator());
                    }
                    bufferedReader.close();
                    fileInputStream.close();
                }
            }

            brout.write("\nNumber of Edges...");
            brout.newLine();
            brout.write(""+edgeCount);

            brout.write("\nNumber of Vertices...");
            brout.newLine();
            brout.write(""+vertices);

            if(fileSystem.exists(tempDir)){
                fileSystem.delete(tempDir,true);
            }

        }

       return 0;
    }

    public static String  getUniqueNameEx(){
        Calendar calender = Calendar.getInstance();
        return calender.get(Calendar.DAY_OF_YEAR)+"_"+calender.get(Calendar.YEAR)+"_"+
                calender.get(Calendar.HOUR_OF_DAY)+"_"+calender.get(Calendar.MINUTE)+"_"+
                calender.get(Calendar.SECOND)+"_"+calender.getTimeInMillis();
    }

    public static void main(String[] args) throws Exception {
        long start = new Date().getTime();
        int res = -1;
        try {
            if(args.length!=3){
                System.out.println("Please provide proper argument..");
                System.out.println("InputDirectory OutputDirectory K-Value");
                return;
            }
            res = ToolRunner.run(new Configuration(), new GraphDriver(), args);
            long end = new Date().getTime();
            System.out.println("Graph Job Took "+(end-start)/60000 + " minutes.");
            System.exit(res);
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            System.exit(res);
        }
    }
}
