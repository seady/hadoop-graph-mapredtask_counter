import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import java.io.IOException;

public class GraphMap extends Mapper<Text,Text,Text,IntWritable> {


    @Override
    protected void setup(Context context) throws IOException, InterruptedException {

    }

    @Override
    protected void map(Text key, Text value, Context context) throws IOException, InterruptedException {
        context.write(key,new IntWritable(1));
        context.write(value,new IntWritable(1));
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {

    }


}
