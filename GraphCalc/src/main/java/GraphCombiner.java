import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;


public class GraphCombiner extends Reducer<Text,IntWritable,Text,IntWritable>{

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {

    }

    @Override
    protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
        int degreeCount = 0;
        for(IntWritable value:values){
            degreeCount+=value.get();
        }
        context.write(key,new IntWritable(degreeCount));
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {

    }

}
